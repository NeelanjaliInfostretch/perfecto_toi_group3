package com.perfecto_toi_group3.pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ProfilePage {
	@FindBy(locator = "btn.city.profilePage")
	private QAFWebElement cityName;

	public QAFWebElement getcityName() {
		return cityName;
		
	}


	@QAFTestStep(description = "user verify the city {0} with {1}")
	public void userVerifyTheCity(String cityloc,String cityname) {

		String citySelected= new QAFExtendedWebElement(cityloc).getText();
		
	if(Validator.verifyThat(cityname, Matchers.containsString(new QAFExtendedWebElement(cityloc).getText()))) 
	{
		Reporter.logWithScreenShot("City name correctly selected and verified."+ citySelected );
	}
	}

}
